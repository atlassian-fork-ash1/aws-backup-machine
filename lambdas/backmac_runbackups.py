#!/usr/bin/env python3

import datetime
import json
import os
import pprint
import time

import boto3

import backmac_notify as notify


def get_instances_to_backup():
    cfn = boto3.resource('cloudformation', region_name=os.environ['AWS_REGION'])
    backup_stack_list = []
    for stack in list(cfn.stacks.all()):
        print(f'Checking: {stack}')
        if {'Key': 'backmac_enabled', 'Value': 'true'} in stack.tags:
            print(f'Adding: {stack} to queue for backup')
            backup_stack_list.append(stack.name)
    return backup_stack_list


def push_backups_to_queue(queue, stack_name):
    stack_list = [stack_name] if stack_name else get_instances_to_backup()
    queue_list = read_sqs_queue(queue)
    print(stack_list)
    print(queue_list)
    if len(queue_list) > 0:
        deduped_list = set(stack_list) - set(queue_list)
    else:
        deduped_list = stack_list
    print(deduped_list)
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    # add stacks in stack_list to the queue to be backed up
    dedup = 0
    for stack in deduped_list:
        dedup += 1
        try:
            queue_response = sqs.send_message(
                QueueUrl=f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/{queue}",
                MessageBody=stack,
            )
            print(queue_response)
        except Exception as e:
            print('type is:', e.__class__.__name__)
            pprint.pprint(e)
            return 'failed'


def get_sfn_state(backmac_sfn):
    # check to see if the sfn is already running. If it is we won't need to execute it.
    sfn = boto3.client('stepfunctions', region_name=os.environ['AWS_REGION'])
    state = sfn.list_executions(stateMachineArn=backmac_sfn, maxResults=1)
    try:
        status = state['executions'][0]['status']
    except:
        status = None
    return status


def read_sqs_queue(queue):
    messages = []
    queue_list = []
    queue_url = f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/{queue}"
    print(queue_url)
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    try:
        message = sqs.receive_message(QueueUrl=queue_url, MaxNumberOfMessages=10)[
            'Messages'
        ]
        while len(message) > 0:
            messages.extend(message)
            try:
                message = sqs.receive_message(
                    QueueUrl=queue_url, MaxNumberOfMessages=10
                )['Messages']
            except KeyError:
                # no more messages
                message = {}
        for stack in messages:
            queue_list.append(stack['Body'])
    except KeyError:
        # no messages
        pass
    return queue_list


def check_queue_state(queue):
    queue_url = f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/{queue}"
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    messages_in_flight = 1
    while int(messages_in_flight) > 0:
        # it takes a while for messages to be visible in the queue again, using a sleep to keep looking until we can see all messages.
        print(f'Messages in flight: {messages_in_flight}')
        time.sleep(1)
        messages_in_flight = sqs.get_queue_attributes(
            QueueUrl=queue_url, AttributeNames=['ApproximateNumberOfMessagesNotVisible']
        )['Attributes']['ApproximateNumberOfMessagesNotVisible']
    return


def start_backup_run(sfn_arn, sfn_input):
    # start stepfunction
    sfn_input_str = json.dumps(sfn_input)
    print(sfn_input_str)
    execution_args = {
        'stateMachineArn': sfn_arn,
        'input': sfn_input_str,
        'name': sfn_input['run_id'],
    }
    sfn = boto3.client('stepfunctions', region_name=os.environ['AWS_REGION'])
    sfn_run = sfn.start_execution(**execution_args)
    return sfn_run


def lambda_handler(event, context):
    # try to pull each possible input from the event; use sensible fallbacks
    try:
        backup_machine = event['backup_machine']
    except KeyError:
        backup_machine = 'backup_machine'

    try:
        dr_region = event['dr_region']
    except KeyError:
        dr_region = os.environ['dr_region']

    try:
        queue_name = event['queue_name']
    except KeyError:
        queue_name = 'backmac-queue'

    try:
        stack = event['stack_name']
    except KeyError:
        stack = None

    # ensure the event has the values derived above
    event['backup_machine'] = backup_machine
    event['dr_region'] = dr_region
    event['queue_name'] = queue_name
    event['run_id'] = datetime.datetime.utcnow().strftime('%Y%m%d-%H%M%S-%f')

    push_backups_to_queue(queue_name, stack)
    # check if sfn is already running. Start it if not.
    backmac_sfn_arn = f"arn:aws:states:{os.environ['AWS_REGION']}:{boto3.client('sts').get_caller_identity().get('Account')}:stateMachine:{backup_machine}"
    state = get_sfn_state(backmac_sfn_arn)
    print(f'Backmac State for {backup_machine}: {state}')
    if state == 'RUNNING':
        # don't start
        print("Not starting the SFN since it's already running")
        message = 'Backmac run queued (SFN already running)'
    else:
        # start
        check_queue_state(queue_name)
        print(start_backup_run(backmac_sfn_arn, event))
        message = 'Backmac run started'

    if stack:
        message = message + f' for {stack}'
    print(notify.send("info", message, event))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"
    os.environ['dr_region'] = "us-east-1"

    event = {"dr_region": "us-east-2"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
