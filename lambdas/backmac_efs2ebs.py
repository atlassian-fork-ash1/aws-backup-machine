#!/usr/bin/env python3

import os
import pprint
import time

import boto3

import backmac_utils


def ssm_send_command(backmac_instance, cmd):
    # note the ExecutionTimeout is set to 6 hours. EAC may well take this long to sync
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd], 'executionTimeout': ["21600"]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print("for command: ", cmd, " command_Id is: ", ssm_command['Command']['CommandId'])
    if ssm_command['ResponseMetadata']['HTTPStatusCode'] == 200:
        return ssm_command['Command']['CommandId']


def ssm_check_command(backmac_instance, cmd_id):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    time.sleep(3)
    cmd_state = ssm.get_command_invocation(
        CommandId=cmd_id, InstanceId=backmac_instance
    )
    print(cmd_state[u'StandardOutputContent'])
    print(cmd_state[u'StandardErrorContent'])
    if cmd_state['ResponseMetadata']['HTTPStatusCode'] == 200:
        return cmd_state[u'ResponseCode']


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    try:
        backmac_instance = event['backmac_instance']
    except:
        raise Exception('No backmac_instance found in event')
    # validate there is some backup storage mounted
    cmd_id = ssm_send_command(backmac_instance, "df -h /backup && df -h /media/atl")
    cmd_rc = ssm_check_command(backmac_instance, cmd_id)
    if cmd_rc == 0:
        # rsync the data from efs to ebs
        cmd_id = ssm_send_command(
            backmac_instance, "/usr/bin/time /usr/local/bin/parallel_sync"
        )
    else:
        event['rsync_status'] = "Failed"
    event['rsync_command_id'] = cmd_id
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_id": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097320",
        "ebs_backup_vol": "vol-00000000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "rds_snap_arn": "arn:aws:rds:us-east-2:000000000000:snapshot:jira-stack-snap-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
