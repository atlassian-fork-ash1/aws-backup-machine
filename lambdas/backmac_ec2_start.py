#!/usr/bin/env python3

import os

import backmac_utils
import boto3
import botocore


def get_exports():
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    return exports_dict


def get_backmac_stack_tags():
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    backmac_stack_tags = cfn.describe_stacks(
        StackName=os.environ['backmac_stack_name']
    )['Stacks'][0]['Tags']
    return backmac_stack_tags


def get_latest_ami_id():
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    latest_ami = ssm.get_parameter(Name=os.environ['latest_ami_ssm_param'])
    return latest_ami['Parameter']['Value']


def launch_ec2(launchtemplate_id, ami_id, event):
    template_dict = {'LaunchTemplateId': launchtemplate_id, 'Version': '$Latest'}
    tags_dict = {
        'ResourceType': 'instance',
        'Tags': [
            {'Key': 'backmac_run_id', 'Value': event['run_id']},
            {'Key': 'backmac_run_stack_name', 'Value': event['stack_name']},
        ],
    }
    for tag in get_backmac_stack_tags():
        if tag['Key'] not in ('Name', 'service_name'):
            tags_dict['Tags'].append(tag)
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    try:
        response = ec2.run_instances(
            LaunchTemplate=template_dict,
            ImageId=ami_id,
            InstanceMarketOptions={'MarketType': 'spot'},
            MinCount=1,
            MaxCount=1,
            TagSpecifications=[tags_dict],
        )
    except botocore.exceptions.ClientError as boto_err:
        if 'no Spot capacity' in boto_err.response['Error']['Message']:
            response = ec2.run_instances(
                LaunchTemplate=template_dict,
                ImageId=ami_id,
                MinCount=1,
                MaxCount=1,
                TagSpecifications=[tags_dict],
            )
        else:
            raise
    return response


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    exports_dict = get_exports()
    backmac_launchtemplate_id = list(
        filter(
            lambda resource: resource['Name'] == 'BackmacNodeLaunchTemplate',
            exports_dict['Exports'],
        )
    )[0]['Value']
    latest_ami_id = get_latest_ami_id()
    backmac_instance = launch_ec2(backmac_launchtemplate_id, latest_ami_id, event)
    print(backmac_instance)
    event['backmac_instance'] = backmac_instance['Instances'][0]['InstanceId']
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"dr_region": "us-east-1", "stack_name": "jira-stack"}
    context = ''
    result = lambda_handler(event, context)
