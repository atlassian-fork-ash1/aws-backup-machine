#!/usr/bin/env python3

import os
import pprint
import sys
from datetime import datetime, timedelta

import backmac_utils
import boto3
from botocore.config import Config


def ensure_rdssnap_complete(rds_snap):
    print(f'Ensuring snapshot complete for: {rds_snap}')
    config = Config(retries=dict(max_attempts=10))
    rds = boto3.client('rds', region_name=os.environ['AWS_REGION'], config=config)
    waiter = rds.get_waiter('db_snapshot_completed')
    waiter.wait(
        DBSnapshotIdentifier=rds_snap, WaiterConfig={'Delay': 30, 'MaxAttempts': 100}
    )
    pprint.pprint(waiter)
    return ()


def copy_rdssnap_todr(event):
    rds_snap = event['rds_snap']
    rds_snap_arn = event['rds_snap_arn']
    dr_region = event['dr_region']
    backup_retention = event['backup_retention']
    tags = [
        {
            'Key': 'backup_lambda_log_group_name',
            'Value': f"{os.environ['AWS_LAMBDA_LOG_GROUP_NAME']}",
        },
        {
            'Key': 'backup_lambda_log_stream_name',
            'Value': f"{os.environ['AWS_LAMBDA_LOG_STREAM_NAME'].replace('/[$LATEST]', '-LATEST-')}",
        },
        {'Key': 'backup_date', 'Value': f'{datetime.now()}'},
        {
            'Key': 'backup_delete_after',
            'Value': f'{datetime.now() + timedelta(backup_retention)}',
        },
    ]
    tags = tags + event['stack_tags']

    print(f'Copying snapshot {rds_snap} {rds_snap_arn} to: {dr_region}')
    config = Config(retries=dict(max_attempts=10))
    rds = boto3.client('rds', region_name=dr_region, config=config)
    try:
        if event['stack_encrypted'] == 'true':
            dr_snap = rds.copy_db_snapshot(
                SourceDBSnapshotIdentifier=rds_snap_arn,
                TargetDBSnapshotIdentifier='dr-' + rds_snap,
                Tags=tags,
                CopyTags=True,
                SourceRegion=os.environ['AWS_REGION'],
                KmsKeyId=event['dr_kms_key_arn'],
            )
        else:
            dr_snap = rds.copy_db_snapshot(
                SourceDBSnapshotIdentifier=rds_snap_arn,
                TargetDBSnapshotIdentifier='dr-' + rds_snap,
                Tags=tags,
                CopyTags=True,
                SourceRegion=os.environ['AWS_REGION'],
            )
    except rds._exceptions.DBSnapshotAlreadyExistsFault as e:
        pprint.pprint(e)
        return 'dr-' + rds_snap
        pass
    except Exception as e:
        print('type is:', e.__class__.__name__)
        pprint.pprint(e)
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        sys.exit(message)

    return dr_snap['DBSnapshot']['DBSnapshotIdentifier']


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    ensure_rdssnap_complete(event['rds_snap'])
    event['rds_dr_snap'] = copy_rdssnap_todr(event)
    print('returning successfully')
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = '2017/09/10/[$LATEST]078e6de35eda4343a4d44002f646831e'
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rds_encrypted": "false",
        "source_kms_key_arn": "",
        "dr_kms_key_arn": "",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
