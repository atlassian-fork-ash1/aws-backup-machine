#!/usr/bin/env python3

import os
import pprint

import backmac_utils
import boto3
from botocore.config import Config


config = Config(retries=dict(max_attempts=10))
EC2 = boto3.resource('ec2', region_name=os.environ['AWS_REGION'], config=config)
EFS = boto3.client('efs', region_name=os.environ['AWS_REGION'])


def add_security_group(backmac_instance, group_id):
    print(
        f'Adding security group {group_id} to backmac instance {backmac_instance.instance_id}'
    )
    grouplist = [d['GroupId'] for d in backmac_instance.security_groups]
    if group_id not in grouplist:
        grouplist.append(group_id)
        backmac_instance.modify_attribute(Groups=grouplist)


def get_used_space(efs_id):
    """adding used space from /media/atl to event for debugging and resizing if required"""
    efs_details = EFS.describe_file_systems(FileSystemId=efs_id)
    used_space = efs_details['FileSystems'][0]['SizeInBytes']
    return int(used_space['Value'] / 1000)


def check_mount_target(backmac_instance, efs_id):
    # TODO make sure the efs has a valid mount target in this AZ, if not, create one.
    mount_targets = EFS.describe_mount_targets(FileSystemId=efs_id)
    mount_targets_subnets = list(
        map(lambda target: target['SubnetId'], mount_targets['MountTargets'])
    )
    print(f'subnets for {efs_id} mount targets: {", ".join(mount_targets_subnets)}')

    if backmac_instance.subnet_id in mount_targets_subnets:
        print('found subnet in mount targets')
    else:
        print("couldn't find subnet in mount targets, need to create")
        raise Exception(
            f'No EFS mount target found in {backmac_instance.placement["AvailabilityZone"]}'
        )


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)

    stack_name = event['stack_name']
    backmac_instance = EC2.Instance(event['backmac_instance'])

    efs_id = backmac_utils.get_stack_resource_id(stack_name, 'ElasticFileSystem')
    event['efs_id'] = efs_id

    security_group_id = backmac_utils.get_stack_resource_id(stack_name, 'SecurityGroup')
    event['security_group_id'] = security_group_id

    check_mount_target(backmac_instance, efs_id)
    add_security_group(backmac_instance, security_group_id)

    print(f'Mounting efs {efs_id} to backmac instance {event["backmac_instance"]}')
    mount_cmd = f'mount -t efs -o tls,ro {efs_id}:/ /media/atl'
    backmac_utils.ssm_wait_response(backmac_instance, mount_cmd)

    event['efs_used_space'] = get_used_space(efs_id)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-west-2", "stack_name": "jira-stack"}

    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
