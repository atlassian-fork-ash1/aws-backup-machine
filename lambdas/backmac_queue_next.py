#!/usr/bin/env python3

import datetime
import json
import os
import pprint

import boto3

import backmac_utils


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)

    # stub out event for next run
    sfn_input = {'run_id': datetime.datetime.utcnow().strftime('%Y%m%d-%H%M%S-%f')}

    # try to pull inputs from the current run; use sensible fallbacks
    try:
        sfn_input['backup_machine'] = event['backup_machine']
    except KeyError:
        sfn_input['backup_machine'] = 'backup_machine'

    try:
        sfn_input['dr_region'] = event['dr_region']
    except KeyError:
        sfn_input['dr_region'] = os.environ['dr_region']

    try:
        sfn_input['queue_name'] = event['queue_name']
    except KeyError:
        sfn_input['queue_name'] = 'backmac-queue'

    try:
        sfn_input['webhook_notification_url'] = event['webhook_notification_url']
    except KeyError:
        # we should only include the webhook url if it was provided; we shouldn't
        # populate this for the next run from the os.environ value for this lambda
        pass

    execution_args = {
        'stateMachineArn': f"arn:aws:states:{os.environ['AWS_REGION']}:{boto3.client('sts').get_caller_identity().get('Account')}:stateMachine:{sfn_input['backup_machine']}",
        'input': json.dumps(sfn_input),
        'name': sfn_input['run_id'],
    }

    sfn = boto3.client('stepfunctions', region_name=os.environ['AWS_REGION'])
    sfn_run = sfn.start_execution(**execution_args)
    print(f"Next backup run queued at {sfn_run[u'startDate']}")
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"dr_region": "us-east-1"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
