#!/usr/bin/env python3

import os
import pprint
import time
from datetime import datetime, timedelta

import boto3

import backmac_utils

from botocore.config import Config

CFN = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])


def get_backup_retention(stack_name):
    tags = CFN.describe_stacks(StackName=stack_name)['Stacks'][0]['Tags']
    backmac_retention = 30
    for tag in tags:
        if tag['Key'] == 'backmac_retention_days':
            backmac_retention = tag['Value']
    try:
        backmac_retention = int(backmac_retention)
    except ValueError:
        print(
            f'The value found for backmac_retention_days ({backmac_retention}) is not an integer'
        )
        exit(1)
    return backmac_retention


def clean_tags(tags):
    # We don't want to overwrite the 'Name' tag, so remove it from the local dict.
    if len(tags) > 0:
        for tag in tags:
            if tag.get('Key') == 'Name':
                del tag['Key']
                del tag['Value']
    return tags


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    stack_name = event['stack_name']
    rds_name = backmac_utils.get_stack_resource_id(stack_name, 'DB')
    event['backup_retention'] = get_backup_retention(stack_name)
    timestamp = time.strftime('%Y%m%d%H%M')
    rds_snap_name = rds_name + '-snap-' + timestamp
    config = Config(retries=dict(max_attempts=10))
    rds = boto3.client('rds', region_name=os.environ['AWS_REGION'], config=config)
    tags = [
        {'Key': 'Name', 'Value': rds_name + '-rds-snap-' + timestamp},
        {'Key': 'stack_name', 'Value': stack_name},
        {
            'Key': 'backup_lambda_log_group_name',
            'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_GROUP_NAME']),
        },
        {
            'Key': 'backup_lambda_log_stream_name',
            'Value': '{}'.format(
                os.environ['AWS_LAMBDA_LOG_STREAM_NAME'].replace(
                    '/[$LATEST]', '-LATEST-'
                )
            ),
        },
        {'Key': 'backup_date', 'Value': '{}'.format(datetime.now())},
        {
            'Key': 'backup_delete_after',
            'Value': '{}'.format(datetime.now() + timedelta(event['backup_retention'])),
        },
    ]
    tags = tags + clean_tags(event['stack_tags'])
    print(tags)
    rds_snap = rds.create_db_snapshot(
        DBInstanceIdentifier=rds_name, DBSnapshotIdentifier=rds_snap_name, Tags=tags
    )
    # add rds details to event
    event['rds_name'] = rds_name
    event['rds_snap'] = rds_snap['DBSnapshot']['DBSnapshotIdentifier']
    event['rds_snap_arn'] = rds_snap['DBSnapshot']['DBSnapshotArn']
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_id": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097320",
        "ebs_backup_vol": "vol-00000000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
