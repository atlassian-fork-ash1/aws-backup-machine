#!/usr/bin/env python3

import json
import os
import time

import backmac_utils
import boto3
from botocore.config import Config


def ssm_wait_response(backmac_instance, cmd):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print('for command:', cmd, 'command_Id is:', ssm_command['Command']['CommandId'])
    status = 'Pending'
    while status in ('Pending', 'InProgress'):
        time.sleep(3)
        list_command = ssm.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command['Commands'][0]['Status']
    ssm_response = ssm.get_command_invocation(
        CommandId=ssm_command['Command']['CommandId'], InstanceId=backmac_instance
    )
    return ssm_response['ResponseCode'], ssm_response['StandardOutputContent']


def ec2_state(instance):
    print(f'Check instance state for: {instance}')
    config = Config(retries=dict(max_attempts=10))
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'], config=config)
    ec2_instance_status = ec2.describe_instance_status(InstanceIds=[instance])
    instance_state = ec2_instance_status['InstanceStatuses'][0]['InstanceState']['Name']
    instance_status = ec2_instance_status['InstanceStatuses'][0]['InstanceStatus'][
        'Status'
    ]
    system_status = ec2_instance_status['InstanceStatuses'][0]['SystemStatus']['Status']
    return instance_state, instance_status, system_status


def cloud_init_successful(instance):
    exit_code, std_out = ssm_wait_response(instance, 'cat /run/cloud-init/result.json')
    if exit_code != 0:
        return False
    try:
        cloud_init_result = json.loads(std_out)
    except ValueError as e:
        print(e)
        return False
    # TODO: remove second phase of conditional; added as workaround for AWS Support case 6795199311
    if cloud_init_result['v1']['errors']:
        if not (
            len(cloud_init_result['v1']['errors']) == 1
            and 'package-update-upgrade-install' in cloud_init_result['v1']['errors'][0]
        ):
            raise Exception(
                'Encountered errors in cloud-init: ', cloud_init_result['v1']['errors']
            )
    return True


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    try:
        backmac_instance = event['backmac_instance']
    except:
        raise Exception('No backmac_instance found in event')

    try:
        ec2_status, instance_status, system_status = ec2_state(backmac_instance)
    except IndexError:
        if 'ec2_status' in event and event['ec2_status'] == 'pending':
            # we've been through the status/waiter loop once already and the instance still has no status? consider this a failure
            raise Exception(
                'EC2 instance failed to start after 2 consecutive status checks'
            )
        else:
            print(
                f'status for instance {backmac_instance} is empty; instance may still be starting'
            )
            ec2_status = 'pending'

    if ec2_status == 'running':
        if instance_status == 'ok' and system_status == 'ok':
            event['ec2_status'] = (
                'ready' if cloud_init_successful(backmac_instance) else 'pending'
            )
        elif instance_status == 'impaired' or system_status == 'impaired':
            event['ec2_status'] = 'failed'
        else:
            event['ec2_status'] = 'pending'
    elif ec2_status == 'pending':
        event['ec2_status'] = 'pending'
    else:
        # something is wrong - state is one of shutting-down, terminated, stopping or stopped
        event['ec2_status'] = 'failed'
        print(ec2_status)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-east-2", "stack_name": "jira-stack"}
    context = ''
    result = lambda_handler(event, context)
