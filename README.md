# aws-backup-machine (backmac)

## What it does

Backmac will add a Cloudwatch rule at the specified cron schedule to trigger the _backmac\_runbackups_ Lambda. This will look for all CloudFormation stacks with the tag "backmac_enabled" set to "true" and add them to an SQS queue before triggering the execution of a State Machine. The State Machine (run once per stack, sequentially) will orchestrate the snapshotting volumes and DB instances in-use by Atlassian services, and the copying of those snapshots to a Disaster Recovery (DR) region.

The Cloudwatch rule will also trigger the snapshot cleaner, which will delete any snapshots older than their tagged 'backup_delete_after' date (defaults to 30 days; configurable via a "backmac_retention_days" tag on each CloudFormation stack targeted for backup).

**NOTE**:

AWS Backup Machine is an unsupported backup solution  designed by Atlassian.  Atlassian takes no responsibility for your use of Backup Machine. The apps are often developed in conjunction with Atlassian hack-a-thons and ShipIt days and are provided to you completely “as-is”, with all bugs and errors, and are not supported by Atlassian. Unless licensed otherwise by Atlassian, Backup Machine is subject to Section 14 of the Cloud Terms of Service – https://www.atlassian.com/legal/cloud-terms-of-service – or Section 8 of the Software License Agreement – https://www.atlassian.com/legal/software-license-agreement, as applicable, and are "No-Charge Products" as defined therein.


## Prerequisites

### Python

Backmac requires Python 3.

### AWS

To deploy Backmac, your local (or CI) environment must be configured with AWS environment variables, and the access keys or IAM role you're using must have enough permissions to upload the Lambdas (if you're rolling your own) and deploy the CloudFormation stack. Generally, you'll need administrator level of access; either the AWS managed "AdministratorAccess" policy or the effective "Allow *" for at least the following services:

* EC2
* IAM
* Lambda
* CloudWatch (Rule and Logs)
* S3
* SQS
* SSM
* States

## Deploying Backmac

### CloudFormation Parameters

A quick overview of the parameters for Backmac's CloudFormation template. We provide sensible defaults where appropriate.

* **BackmacDRRegion**
  The region that snapshots of EFS and EBS volumes and RDS instances will be copied to.

* **BackupSchedule**
  A cron expression which controls how often Backmac will run (note: execution times are UTC).

* **NodeInstanceType**
  Backmac uses an EC2 node as an interface to EFS volumes for snapshotting (via an EBS volume); it will use this instance type. We recommend instances with high network and EBS throughputs.

* **AccessCIDR**
  The CIDR IP range that is permitted to access Backmac's EC2 instance(s).

* **AssociatePublicIpAddress**
  Controls whether or not Backmac's EC2 instances will be assigned a public IP address.

* **InternalSubnet**
  The subnet in your VPC that Backmac will operate in. Currently, Backmac only supports being deployed in a single subnet/AZ.

* **KeyName**
  The EC2 Key Pair to allow SSH access to the instances.

* **VPC**
  The VPC that Backmac will operate in.

* **WebhookNotificationURL**
  A webhook for sending notifications of Backmac's progress for each run (successes, failures, etc). Currently, this field supports either a Slack webhook URL (see "Slack Notifications" below), or any generic webhook which expects a JSON payload with either "text" or "message" fields.

* **S3LambdaBucketPrefix**
  If using a customized set of Lambdas, this should be set to the name of the bucket they have been uploaded to.

* **S3LambdaBucketFolder**
  If using a customized set of Lambdas, this should be set to the folder/path in the bucket specified by _S3LambdaBucketPrefix_ where the Lambdas have been uploaded to.

### Deploying custom lambdas

Two notes:

1. The steps below are _not_ necessary to use Backmac – you can use the publicly-accessible Lambdas published by this repo by using the default locations for _S3LambdaBucketPrefix_ and _S3LambdaBucketFolder_ in the CloudFormation template.
2. The following instructions require [cloudtoken][1] or the [AWS CLI][2] to be configured.

Run the _package.sh_ script optionally stating the region and the bucket name and path for the Lambdas to be uploaded to (defaults to "us-east-1" and "wpe-backmac-deployment-${aws_account_id}-${region}"):

```commandline
./package.sh "us-east-2" "my-awesome-bucket-us-east-2/lambdas/go/here"
```

This will zip and upload all the Lambdas to the specified bucket, making them available for use by the CloudFormation template. Note that currently, in addition to specifying the region, the bucket name must also end with the region. However, when suppling these values to the CloudFormation template, the region should be dropped from the bucket name, e.g.:

* _S3LambdaBucketPrefix_: my-awesome-bucket
* _S3LambdaBucketFolder_: lambdas/go/here

### Slack Notifications

Backmac supports sending Slack-specific webhook payloads if a Slack webhook URL is provided. These messages will contain more information than the standard webhook payload (e.g. duration of state machine run, stack traces from failures, links to view CloudWatch logs, etc). No extra Backmac configuration is necessary to enable these features, but a slight amount of work is necessary on the Slack side of things:

1. You'll need a webhook URL, either by creating a Slack App or using a standard "Incoming WebHook" integration.
2. You'll need to configure the following emoji in your Slack instance (default icons provided in the _assets_ folder of this repo):
    * :aws_cloudwatch:
    * :aws_lambda:
    * :aws_stepfunctions:
    * :backmac_failure:
    * :backmac_info:
    * :backmac_success:
    * :backmac_warning:

### Warning

Backmac will not follow symlinks. If you need to backup things that are symlinked you may want to fork and modify the parallel_sync script.

[1]: https://bitbucket.org/atlassian/cloudtoken
[2]: https://aws.amazon.com/cli/
