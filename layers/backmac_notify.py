#!/usr/bin/env python3

import json
import os
import re
import sys
import time
import traceback

import boto3

from botocore.vendored import requests


AWS_ACCOUNT_ID = boto3.client("sts").get_caller_identity().get("Account")


def generate_pretty_duration(start_time, end_time):
    '''Given two unix timestamps, produces a duration broken out by hours, minutes, and seconds; drops leading zeros and units equal to zero'''
    duration_values = time.strftime(
        "%-H|%-M|%-S", time.gmtime(end_time - start_time)
    ).split('|')
    duration_with_units = []
    for i, unit in enumerate(['h', 'm', 's']):
        if duration_values[i] != '0':
            duration_with_units.append(f'{duration_values[i]}{unit}')
    return ' '.join(duration_with_units)


def build_machine_url(state_machine_name):
    '''builds a URL to a given state machine in the AWS Console'''
    return (
        f'https://{os.environ["AWS_REGION"]}.console.aws.amazon.com/states/home?region={os.environ["AWS_REGION"]}'
        f'#/statemachines/view/arn:aws:states:{os.environ["AWS_REGION"]}:{AWS_ACCOUNT_ID}:stateMachine:{state_machine_name}'
    )


def build_execution_url(state_machine_name, execution_id):
    '''builds a URL to an execution for a state_machine in the AWS Console'''
    return (
        f'https://{os.environ["AWS_REGION"]}.console.aws.amazon.com/states/home?region={os.environ["AWS_REGION"]}'
        f'#/executions/details/arn:aws:states:{os.environ["AWS_REGION"]}:{AWS_ACCOUNT_ID}:execution:{state_machine_name}:{execution_id}'
    )


def send_generic_webhook(message_type, message_text, webhook_url, event):
    '''Sends a basic webhook with "message" and "text" params (compatible with many services)'''
    message = f'[{message_type.upper()}] - {os.environ["AWS_REGION"]}: {message_text}'
    if message_type == 'failure':
        try:
            message = message + f' ({event["error_details"]["error"]})'
        except KeyError:
            pass
    if 'backup_machine' in event and 'run_id' in event:
        message = (
            message
            + f' (view run: {build_execution_url(event["backup_machine"], event["run_id"])})'
        )
    webhook_data = {'message': message, 'text': message}
    print(json.dumps(webhook_data))
    response = requests.post(
        webhook_url,
        data=json.dumps(webhook_data),
        headers={'Content-Type': 'application/json'},
    )
    return response


def send_slack_webhook(message_type, message_text, webhook_url, event):
    '''Sends a Slack-specific webhook with nice formatting for extra information'''

    # look for the stack name in the event message; make it bold by wrapping with asterisks
    if 'stack_name' in event:
        message_text = re.sub(
            rf'(.*)\b({event["stack_name"]})\b(.*)', r'\1*\2*\3', message_text
        )

    # stub out the message
    message = f':backmac_{message_type}:  {message_text}'

    # if this is a failure, try to add the error type to the message
    if message_type == 'failure':
        try:
            message = message + f' ({event["error_details"]["error"]})'
        except KeyError:
            pass

    # stub out the webhook data structure
    webhook_data = {
        'blocks': [{'type': 'section', 'text': {'type': 'mrkdwn', 'text': message}}]
    }

    # add various links and actions (if we have them) to an overflow_menu
    overflow_menu = []
    if 'backup_machine' in event and 'run_id' in event and 'end_tstamp' in event:
        overflow_menu.append(
            {
                'text': {
                    'type': 'plain_text',
                    'text': ':aws_stepfunctions:  View Run',
                    'emoji': True,
                },
                'url': build_execution_url(event['backup_machine'], event['run_id']),
            }
        )
    if message_type == 'failure':
        try:
            overflow_menu.append(
                {
                    'text': {
                        'type': 'plain_text',
                        'text': ':aws_cloudwatch:  View Logs',
                        'emoji': True,
                    },
                    'url': f'https://{os.environ["AWS_REGION"]}.console.aws.amazon.com/cloudwatch/home?region={os.environ["AWS_REGION"]}#logEventViewer:group={event["error_details"]["log_group_name"]};stream={event["error_details"]["log_stream_name"]}',
                }
            )
            overflow_menu.append(
                {
                    'text': {
                        'type': 'plain_text',
                        'text': ':aws_lambda:  View Lambda',
                        'emoji': True,
                    },
                    'url': f'https://{os.environ["AWS_REGION"]}.console.aws.amazon.com/lambda/home?region={os.environ["AWS_REGION"]}#/functions/{event["error_details"]["function_name"]}?tab=graph',
                }
            )
        except KeyError:
            pass

    # add assembled overflow items to message
    if len(overflow_menu) > 1:
        webhook_data['blocks'][0]['accessory'] = {
            'type': 'overflow',
            'options': overflow_menu,
        }
    elif len(overflow_menu) == 1:
        # overflow menus must have more than 1 item; convert to button if we only have 1
        overflow_menu[0]['type'] = 'button'
        webhook_data['blocks'][0]['accessory'] = overflow_menu[0]

    # look for error details we can add to the message
    if message_type == 'failure':
        try:
            error_message = event['error_details']['cause']['errorMessage']
        except TypeError:
            error_message = event['error_details']['cause']
        except KeyError:
            error_message = 'Unknown error; check SFN logs'

        webhook_data['blocks'].append(
            {
                'type': 'section',
                'text': {'type': 'mrkdwn', 'text': f'*Error Details*\n{error_message}'},
            }
        )

        try:
            stacktrace_lines = []
            for err in event['error_details']['cause']['stackTrace']:
                stacktrace_lines.append(' '.join(map(str, err)))
            webhook_data['blocks'].append(
                {
                    'type': 'section',
                    'text': {
                        'type': 'mrkdwn',
                        'text': '*Stack Trace*\n```{}```'.format(
                            '\n'.join(stacktrace_lines)
                        ),
                    },
                }
            )
        except:
            pass

    # add various facts about the run (if we have them) as context elements
    context_elements = []
    context_elements.append(
        {'type': 'mrkdwn', 'text': f'Region: *{os.environ["AWS_REGION"]}*'}
    )
    if 'dr_region' in event:
        context_elements.append(
            {'type': 'mrkdwn', 'text': f'DR Region: *{event["dr_region"]}*'}
        )
    if 'backup_machine' in event and event['backup_machine'] != 'backup_machine':
        context_elements.append(
            {
                'type': 'mrkdwn',
                'text': f'Machine: *<{build_machine_url(event["backup_machine"])}|{event["backup_machine"]}>*',
            }
        )
    if 'start_tstamp' in event and 'end_tstamp' in event:
        context_elements.append(
            {
                'type': 'mrkdwn',
                'text': f'Duration: *{generate_pretty_duration(event["start_tstamp"], event["end_tstamp"])}*',
            }
        )

    # add assembled context elements to message
    if context_elements:
        webhook_data['blocks'].append({'type': 'context', 'elements': context_elements})

    print(json.dumps(webhook_data))
    response = requests.post(
        webhook_url,
        data=json.dumps(webhook_data),
        headers={'Content-Type': 'application/json'},
    )
    return response


def send(message_type, message_text, event):
    if event.get('webhook_notification_url'):
        webhook_notification_url = event.get('webhook_notification_url')
    elif os.environ.get('webhook_notification_url'):
        webhook_notification_url = os.environ.get('webhook_notification_url')
    else:
        print('No webhook_notification_url defined, notifications will not be sent')
        return

    print('sending webhook')
    try:
        if 'hooks.slack.com' in webhook_notification_url:
            response = send_slack_webhook(
                message_type, message_text, webhook_notification_url, event
            )
        else:
            response = send_generic_webhook(
                message_type, message_text, webhook_notification_url, event
            )
        response.raise_for_status()
        result = ' '.join([str(response.status_code), response.text])
    except:
        print('webhook sending failed (not fatal); returning traceback and continuing')
        result = ''.join(traceback.format_exception(*sys.exc_info()))

    return result
